package virtalab.sys;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
/**
 * Java version of common shell functions
 * 
 * @author Alex Muravya (aka kyberorg) <asm@virtalab.net>
 *
 */
public final class Shell {
	
	private Shell(){}
	/**
	 * Reads filename and return it contents
	 * 
	 * @param file filename
	 * @return File contents
	 * @throws IOException when no such file
	 */
	public static String cat(String file) throws IOException{
		File f = Shell.name2file(file);
		return Shell.cat(f);
	}
	/**
	 * Reads file and return it contents
	 * 
	 * @param file filename
	 * @return file content
	 * @throws IOException 
	 */
	@SuppressWarnings("deprecation")
	public static String cat(File file) throws IOException{
		if(!file.canRead()){
			throw new IOException("File or directory is not readable");
		}
		if(file.isDirectory()){
			throw new IOException("Cannot cat directory");
		}
		
		//init
		FileInputStream fis = new FileInputStream(file);
		BufferedInputStream bis = new BufferedInputStream(fis);
		DataInputStream dis = new DataInputStream(bis);
		StringBuffer sb = new StringBuffer();
		
		while (dis.available() != 0){
			sb.append(dis.readLine());
		}
		
		//close
		fis.close();
		bis.close();
		dis.close();
		
		return sb.toString();
	}
	/**
	 * Echos line to stdOut (alias to System.out.println)
	 * 
	 * @param line Line to print 
	 */
	public static void echo(Object line){
		System.out.println(line);
	}
	/**
	 * Puts line "str" to file with "dst" file name (overwrites file)
	 * 
	 * @param str line to put
	 * @param dst destination file name
	 * @throws IOException 
	 */
	public static void echo(Object str,String dst) throws IOException{
		File f = Shell.name2file(dst);
		Shell.echo(str, f, false);
	}
	/**
	 * Puts line "str" to file with "dst" file name (with or without append)
	 * 
	 * @param str Line to put
	 * @param dst Destination filename
	 * @param append When TRUE - adds line to file, FALSE - overwrite file
	 * @throws IOException 
	 */
	public static void echo(Object str,String dst, boolean append) throws IOException{
		File f = Shell.name2file(dst);
		Shell.echo(str, f, append);
	}
	
	/**
	 * Puts line "str" to file with "dst" file (overwrites file)
	 *  
	 * @param str line to put
	 * @param dst destination file name
	 * @throws IOException with Reason why
	 */
	public static void echo(Object str,File dst) throws IOException{
		Shell.echo(str, dst, false);
	}
	/**
	 * Puts line "str" to file with "dst" file name (with or without append)
	 * 
	 * @param str Line to put
	 * @param dst Destination filename
	 * @param append When TRUE - adds line to file, FALSE - overwrite file
	 * @throws IOException with Reason why
	 */
	public static void echo(Object str,File dst, boolean append) throws IOException{
		//check if valid file
		if(!dst.exists()){
			dst.createNewFile();
		}

		if(!dst.canWrite()){
			throw new IOException("File is not writeable");
		}
		if(dst.isDirectory()){
			throw new IOException("Cannot echo to directory");
		}
		
		//init
		FileWriter writer = new FileWriter(dst,append);
		String terminator = System.getProperty("line.separator");
		writer.write(str.toString()+terminator);
		
		writer.close();
		
	}
	/**
	 * Echos line to stdErr (alias to System.err.println)
	 * 
	 * @param line Line to print 
	 */
	public static void eecho(Object line){
		System.err.println(line);
	}
	
	/**
	 * Executes foreground command
	 * 
	 * @param cmd command to execute
	 * @return Output of executed command
	 * @throws IOException when failed to grab output
	 * @throws InterruptedException when process stopped
	 */
	public static String exec(String cmd) throws IOException, InterruptedException{
		if(cmd==null)return "";
			String lineSeparator = System.getProperty("line.separator");
			//System.out.println("exec "+cmd);
			Process proc = Runtime.getRuntime().exec(cmd);
				proc.waitFor();
				BufferedReader reader = new BufferedReader(new InputStreamReader(
						proc.getInputStream()));
	 
				StringBuffer sb = new StringBuffer();
	 
				String line = reader.readLine();
				sb.append(line+lineSeparator);
				while (line != null) {
					line = reader.readLine();
					sb.append(line+lineSeparator);
				}
		return sb.toString();	
	}
	/**
	 * Executes background process
	 * 
	 * @param cmd command and arguments to execute
	 * @throws IOException when something not found
	 */
	public static void execBg(String[] cmd) throws IOException{
		if(cmd==null) return ;
		
		ProcessBuilder pb = new ProcessBuilder(cmd);
		pb.start();
	}
	
	/**
	 * Usage: grep("word*" file)
	 * 
	 * Origin Idea and Source: http://introcs.cs.princeton.edu/java/72regular/Grep.java.html (re taken from here) 
	 * 
	 * @category untested
	 * @param regexp Reg Exp
	 * @param file Search Target File
	 * @return matched strings
	 * @throws IOException when file is not readable or file is Directory
	 */
	@SuppressWarnings("deprecation")
	public static StringBuffer grep(String regexp, File file) throws IOException{
		
		if(!file.canRead()){
			throw new IOException("File or directory is not readable");
		}
		if(file.isDirectory()){
			throw new IOException("Cannot cat directory");
		}
		
		//init
		FileInputStream fis = new FileInputStream(file);
		BufferedInputStream bis = new BufferedInputStream(fis);
		DataInputStream dis = new DataInputStream(bis);
		StringBuffer sb = new StringBuffer();
		String re = ".*" + regexp + ".*";
		
		while (dis.available() != 0){
			String line = dis.readLine();
			if(line.matches(re)) sb.append(line);
		}
		
		//close
		fis.close();
		bis.close();
		dis.close();
		
		return sb;
		
	}

	/**
	 * Deletes file or directory
	 * 
	 * @param file filename
	 * @return true-delete success, false - delete failed
	 * @throws IOException when file not found or write protected
	 */
	public static boolean rm(String file) throws IOException{
		File f = Shell.name2file(file);
		return Shell.rm(f);
	}
	/**
	 * Deletes file or directory
	 * 
	 * @param file File object
	 * @return true-delete success, false - delete failed
	 * @throws IOException when file not found or write protected
	 */
	public static boolean rm(File file) throws IOException{
		if(!file.exists()){
			throw new FileNotFoundException("No such file or directory");
		}
		if(!file.canWrite()){
			throw new IOException("Cannot delete: file write protected");
		}
		if(file.isDirectory()){
			String[] files = file.list();
			if(files.length > 0 ){
				throw new IOException("Directory is not empty");
			}
		}
		boolean rslt = file.delete();
		return rslt;
	}
	/**
	 * Counts lines in file
	 * 
	 * @param file filename
	 * @return number of lines
	 * @throws IOException when file or directory is not readable
	 */
	public static int wc(String file) throws IOException{
		File f = Shell.name2file(file);
		return Shell.wc(f);
	}
	/**
	 * Counts lines in file
	 * 
	 * @param file File Object
	 * @return number of lines
	 * @throws IOException when file or directory is not readable
	 */
	public static int wc(File file) throws IOException{
		if(!file.canRead()){
			throw new IOException("File or directory is not readable");
		}
		if(file.isDirectory()){
			String[] files = file.list();
			return files.length;
		}
		
		//init
		FileInputStream fis = new FileInputStream(file);
		BufferedInputStream bis = new BufferedInputStream(fis);
		DataInputStream dis = new DataInputStream(bis);
		int cnt = 0;
		
		while (dis.available() != 0){
			cnt++;
		}
		
		//close
		fis.close();
		bis.close();
		dis.close();
		
		return cnt;
	}
	
	/**
	 * Creates File Object
	 * 
	 * @param filename Name of file
	 * @return File File Object
	 * @throws IOException when no such file
	 */
	static File name2file(String filename) throws IOException{
		return new File(filename);
	}
}

package virtalab.sys;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
/**
 * Builds command for future execution
 * 
 * @author Alex Muravya (aka kyberorg) <asm@virtalab.net>
 *
 */
public class CommandBuilder {
	private String program;
	private String args;
	private String stdOut;
	private boolean appendStdOut;
	private String stdErr;
	private boolean appendStdErr;
	private boolean isBackgroundProccess;
	private boolean detectProblems;
	private String command;

	/**
	 * Object constructor sets default values to private properties
	 * 
	 */
	public CommandBuilder(){
		this.program=null;
		this.args=null;
		this.stdOut=null;
		this.stdErr=null;
		this.isBackgroundProccess=false;
		this.command=null;
		this.detectProblems=false;
	}

	/**
	 * Sets Program for execution
	 * 
	 * Example: setProgram(you_shell_script.sh);
	 * 
	 * @param program executable file 
	 * @throws IOException when passed wrong filename or file is not executable
	 */
	public void setProgram(String program) throws IOException{
		//if detect, check on exec
		if(this.detectProblems){
			File f = Shell.name2file(program);
			if(f.canExecute()){
				this.program=program;
			} else {
				throw new IOException("Programm is not executable");
			}
		} else {
			this.program = program;
		}
	}
	/**
	 * Sets Program arguments
	 * 
	 * @param args program arguments as single string 
	 */
	public void setArgs(String args){
		this.args=args;
	}
	/**
	 * Sets Program arguments
	 * 
	 * @param args arguments as array of Strings
	 */
	public void setArgs(String[] args){
		StringBuilder sb = new StringBuilder();
		
		for(int i=0;i<args.length;i++){
			sb.append(args[i]+" ");
		}
		this.args=sb.toString();
	}
	/**
	 * Sets file where StdOut will redirected 
	 * 
	 * @param filename name of writable file
	 * @throws IOException when file is not found
	 */
	public void setOutputStream(String filename) throws IOException{
		if(detectProblems){
			File f = Shell.name2file(filename);
			if(f.isFile()){
				this.stdOut= filename;
			} else {
				//TODO check on writable file
				throw new IOException("No such file");
			}
		} else {
			this.stdOut = filename;
		}
	}
	
	/**
	 * Sets append mode for stdOut
	 * 
	 * @param append true-add new lines to file, false - overwrite file
	 */
	public void setAppendOutputStream(boolean append){
		this.appendStdOut=append;
	}
	
	/**
	 * Sets file where StdOut will redirected 
	 * 
	 * @param filename name of writable file
	 * @throws IOException when file is not found
	 */
	public void setErrorStream(String filename) throws IOException{
		if(detectProblems){
			File f = Shell.name2file(filename);
			if(f.isFile()){
				this.stdErr= filename;
			} else {
				//TODO check on writable file
				throw new IOException("No such file");
			}
		} else {
			this.stdErr = filename;
		}
	}
	/**
	 * Sets append mode for stdErr
	 * 
	 * @param append true-add new lines to file, false - overwrite file
	 */
	public void setAppendErrorStream(boolean append){
		this.appendStdErr = append;
	}
	
	/**
	 * Sets Problem Detection mode
	 * 
	 * @param detect Detect problems with absend or non-exec files (default : false);
	 */
	public void setProblemDetector(boolean detect){
		this.detectProblems=detect;
	}
	
	/**
	 * Sets wherever process should be backgrounded or foregrounded  
	 * 
	 * @param isBackgroundProccess true-background process, false - foreground process
	 */
	public void setBackgroundProccess(boolean isBackgroundProccess){
		this.isBackgroundProccess = isBackgroundProccess;
	}
	/**
	 * Makes command as single string for Runtime.exec()
	 * 
	 * @return Command as single space-delimetered string 
	 * @throws IllegalArgumentException when program is null
	 */
	public String getCommand() throws IllegalArgumentException{
		this.makeCommand();
		return this.command;
	}
	/**
	 * Makes command for ProcessBuilder
	 * 
	 * @return String Array
	 */
	public String[] getPbCommand(){
		this.makeCommand();
		return this.toArray(this.command);
	}
	/**
	 * Makes command for Jar files
	 * 
	 * @param jarFile valid Jar filename (with .jar)
	 * @return String Array valid for ProcessBuilder
	 * @throws IOException when Jar file is not valid
	 */
	public String[] getJarCommand(String jarFile) throws IOException{
		this.program = "java ";
		if(this.detectProblems){
			File jar = Shell.name2file(jarFile);
			if(jar.isFile() && jarFile.endsWith("jar")){
				this.args="-jar "+jarFile;
			} else {
				throw new FileNotFoundException("No such jar file or invalid name (must end with .jar");
			}
		} else {
			this.args="-jar "+jarFile;
		}
		this.makeCommand();
		return this.toArray(this.command);
	}
	/**
	 * Returns text of currently generated command
	 * 
	 * @return text of command
	 */
	public String showCommand(){
		return this.command;
	}
	/**
	 * OS Selector
	 */
	private void makeCommand(){
		//select os
		String os = Env.getMyOS();
		if(os.equals("win")){
			this.command = this.makeWinCmd();
		} else {
			this.command = this.makeUnixCmd();
		}
	}
	/**
	 * Unix builder (also valid for mac,solaris,bsd)
	 * @return command
	 */
	private String makeUnixCmd(){
		//prog args 1>file 2>file &
		StringBuilder sb = new StringBuilder();
		
		//program
		if(this.program==null){
			throw new IllegalArgumentException("Program is empty. Cannot build command");
		} else {
			sb.append(this.program +" ");
		}
		//args
		if(this.args!=null){
			sb.append(this.args + " ");
		}
		//stdOut
		if(this.stdOut!=null){
		
		String outSign=null;
		
		if(this.appendStdOut){
			outSign = ">>";
		} else {
			outSign = ">";
		}
		
		sb.append("1"+outSign+this.stdOut+" ");
		
		}
		
		//stdErr
		if(this.stdErr!=null){
		
		String errSign=null;
		
		if(this.appendStdErr){
			errSign = ">>";
		} else {
			errSign = ">";
		}
		
		sb.append("2"+errSign+this.stdErr);
		
		}
		
		if(this.isBackgroundProccess){
			sb.insert(0, "nohup ");
			sb.append("&");
		}
		
		return sb.toString();
	}
	/**
	 * Makes windows command
	 * @return windows command
	 */
	private String makeWinCmd(){
		//start prog args 1>file 2>file
		StringBuilder sb = new StringBuilder();
		
		//background
		if(this.isBackgroundProccess){
			//Not working in Windows (it is always background)
			//TODO make fix for Win Systems (task with small priority)
			//sb.append("start ");
		}
		
		//program
		if(this.program==null){
			throw new IllegalArgumentException("Program is empty. Cannot build command");
		} else {
			sb.append(this.program +" ");
		}
		//args
		if(this.args!=null){
			sb.append(this.args + " ");
		}
		//stdOut
		if(this.stdOut!=null){
		
		String outSign=null;
		
		if(this.appendStdOut){
			outSign = ">>";
		} else {
			outSign = ">";
		}
		
		sb.append("1"+outSign+this.stdOut+" ");
		
		}
		
		//stdErr
		if(this.stdErr!=null){
		
		String errSign=null;
		
		if(this.appendStdErr){
			errSign = ">>";
		} else {
			errSign = ">";
		}
		
		sb.append("2"+errSign+this.stdErr);
		
		}
		
		return sb.toString();
	}
	/**
	 * String->String Array
	 * @param str String
	 * @return String Array
	 */
	private String[] toArray(String str){
	 return str.split(" ");
	}
	
}

package virtalab.sys;

import java.lang.management.ManagementFactory;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Enumeration;
import java.util.Iterator;

import virtalab.regexp.RegExps;


/**
 * Runtime Enviroment
 * @author Alex Muravya (aka kyberorg) <asm@virtalab.net>
 *
 */
public final class Env {
	
	private Env(){}
	/**
	 * Returns this Java program Proccess ID
	 * 
	 * @return Proccess ID (aka pid)
	 */
	public static int getMyPid(){
		String rawPid=ManagementFactory.getRuntimeMXBean().getName();
        String pid=rawPid.replaceAll("[^0-9 ]","");
        return Integer.parseInt(pid);
	}
	/**
	 * Returns current OS String 
	 * 
	 * @return win-Windows, mac-Mac OS,unix - Linux,Unix,BSD,Aix, full marker when match not found.
	 */
	public static String getMyOS(){
		String os = System.getProperty("os.name").toLowerCase();
		if(os!=null){
		if(os.indexOf("win") >= 0){
			return "win";
		} else if(os.indexOf("mac") >= 0){
			return "mac";
		} else if(os.indexOf("nix") >= 0 || os.indexOf("nux") >= 0 || os.indexOf("aix") > 0 ){
			return "unix";
		} else {
			return os;
		}
		} else {
			return "";
		}
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static ArrayList<String> getMyIps(String ver) throws SocketException{
		Enumeration nets = NetworkInterface.getNetworkInterfaces();
		ArrayList<String> ips = new ArrayList<String>();
        for (Iterator it = Collections.list(nets).iterator(); it.hasNext();) {
            NetworkInterface netint = (NetworkInterface) it.next();            
            Enumeration inetAddresses = netint.getInetAddresses();
            for (Iterator iter = Collections.list(inetAddresses).iterator(); iter.hasNext();) {
                //ip
            	InetAddress inetAddress = (InetAddress) iter.next();
                String ip = inetAddress.toString();
                ip = ip.replaceFirst("/","");
                if(ver.equals("ipv4")){
                	if(RegExps.IPv4(ip)){
                     	ips.add(ip);
                     }
                } else {
                	ips.add(ip);
                }
            }
        }
        return ips;
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static void getIFaceInfo() throws SocketException{
		Enumeration nets = NetworkInterface.getNetworkInterfaces();
        for (Iterator it = Collections.list(nets).iterator(); it.hasNext();) {
            NetworkInterface netint = (NetworkInterface) it.next();
            System.out.printf("Display name: %s\n", netint.getDisplayName());
            System.out.printf("Name: %s\n", netint.getName());
            Enumeration inetAddresses = netint.getInetAddresses();
            for (Iterator iter = Collections.list(inetAddresses).iterator(); iter.hasNext();) {
                InetAddress inetAddress = (InetAddress) iter.next();
                System.out.printf("InetAddress: %s\n", inetAddress);
            }
            System.out.printf("\n");
        }
	}
	
}

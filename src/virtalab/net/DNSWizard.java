package virtalab.net;

import java.net.InetAddress;
import java.net.UnknownHostException;


/**
 * Does forward Name->IP resolve and backward IP->Name resolve
 *
 * @author Alex Muravya (aka kyberorg) <asm at virtalab.net>
 *
 */
public final class DNSWizard {
	/**
	 * Resolves hostname to IP address
	 *
	 * @param hostname hostname as String
	 * @return IP addr as String or null, when couldn't resolve
	 */
	public static String name2ip(String hostname){
		InetAddress addr = null;
		try{
			addr = InetAddress.getByName(hostname);
		}catch(UnknownHostException e){
			return null;
		}
		return addr.getHostAddress();
	}

	/**
	 * Resolves IPaddr to DNS name
	 *
	 * @param ip valid IP as String
	 * @return hostname or null (if resolve was not successful)
	 */
	public static String ip2name(String ip){

		InetAddress addr = null;
		String host = null;
		try{
			addr = InetAddress.getByName(ip);
			host = addr.getHostName();
		}catch(UnknownHostException e){
			return null;
		}
		return host;

	}

}

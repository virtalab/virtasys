package virtalab.regexp;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RegExps {

	/**
	 * Check if ip is valid ipv4 address
	 * @param ip to check
	 */
	public static boolean IPv4(String ip) {
		String IPADDRESS_PATTERN =
				"^([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\." +
				"([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\." +
				"([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\." +
				"([01]?\\d\\d?|2[0-4]\\d|25[0-5])$";

		Pattern pattern = Pattern.compile(IPADDRESS_PATTERN);
		Matcher matcher = pattern.matcher(ip);

		return matcher.matches();

	}

}
